package glenn.gasesframeworkthermalexpansion;

import glenn.gasesframework.api.GasesFrameworkAPI;
import net.minecraft.block.Block;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = GasesFrameworkThermalExpansion.MODID, name = "Gases Framework Thermal Expansion Plugin", version = GasesFrameworkThermalExpansion.VERSION, dependencies="required-after:gasesFramework", acceptedMinecraftVersions = "[" + GasesFrameworkThermalExpansion.TARGETVERSION + "]")
public class GasesFrameworkThermalExpansion
{
	@Instance(GasesFrameworkThermalExpansion.MODID)
	public static GasesFrameworkThermalExpansion instance;
	
	public static final String MODID = "gasesFrameworkThermalExpansion";
	public static final String VERSION = "1.0.0";
	public static final String TARGETVERSION = "1.7.10";
	
	public static GasesFrameworkThermalExpansionMainConfigurations configurations;

	public static Block gasDynamo;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		configurations = new GasesFrameworkThermalExpansionMainConfigurations(event.getSuggestedConfigurationFile());
		
		GameRegistry.registerBlock(gasDynamo = new BlockGasDynamo().setHardness(2.5F).setStepSound(Block.soundTypeStone).setCreativeTab(GasesFrameworkAPI.creativeTab).setBlockName("gf_gasDynamo").setBlockTextureName("gasesframeworkthermalexpansion:gas_dynamo"), "gf_gasDynamo");
		GameRegistry.registerTileEntity(TileEntityGasDynamo.class, "gasDynamo");
	}
}