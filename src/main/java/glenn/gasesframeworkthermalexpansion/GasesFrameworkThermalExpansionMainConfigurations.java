package glenn.gasesframeworkthermalexpansion;

import java.io.File;

import glenn.moddingutils.Configurations;

public class GasesFrameworkThermalExpansionMainConfigurations extends Configurations
{
	@ConfigField(path="blocks.Gas Dynamo.Max fuel", comment="The maximal amount of fuel contained by a gas dynamo", defaultValue="1000")
	public int gasDynamo_maxFuel;
	
	@ConfigField(path="blocks.Gas Dynamo.Energy per fuel", comment="The energy generated for each fuel unit", defaultValue="16")
	public int gasDynamo_energyPerFuel;
	
	@ConfigField(path="blocks.Gas Dynamo.Max energy", comment="The maximal amount of energy contained by a gas dynamo", defaultValue="60000")
	public int gasDynamo_maxEnergy;
	
	@ConfigField(path="blocks.Gas Dynamo.Max energy transfer", comment="The maximal amount of energy that can be transmitted from each side of a gas dynamo", defaultValue="80")
	public int gasDynamo_maxEnergyTransfer;
	
	
	
	public GasesFrameworkThermalExpansionMainConfigurations(File configurationsFile)
	{
		super(configurationsFile);
	}

	@Override
	protected void onLoaded()
	{
		
	}
}